<?php

namespace ProPhp\Wildcard;

class Wildcard
{
    public static function isMatching(string $string, string $wildcard): bool
    {
        return fnmatch($wildcard, $string);
    }

    public static function filterIndexedArray(
        array  $array,
        array  $includedWildcards = [],
        array  $excludedWildcards = [],
        string $arrayColumn = null,
        bool   $reindex = true
    ): array
    {
        $result = $array;

        if(count($includedWildcards) > 0){
            $result = array_filter($result, function ($item) use ($includedWildcards, $arrayColumn) {
                $item = $arrayColumn === null ? $item : $item[$arrayColumn];
                foreach ($includedWildcards as $includedWildcard) {
                    if (self::isMatching($item, $includedWildcard)) {
                        return true;
                    }
                }
                return false;
            });
        }

        if(count($excludedWildcards) > 0) {
            $result = array_filter($result, function ($item) use ($excludedWildcards, $arrayColumn) {
                $item = $arrayColumn === null ? $item : $item[$arrayColumn];
                foreach ($excludedWildcards as $excludedWildcard) {
                    if (self::isMatching($item, $excludedWildcard)) {
                        return false;
                    }
                }
                return true;
            });
        }

        return $reindex ? array_values($result) : $result;
    }
}