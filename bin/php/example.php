<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use ProPhp\Wildcard\Wildcard;


# @Wildcard::isMatching()
$testData = [
    ["/test/me/please", '*', true],
    ["/test/me/please", '/test/me/please', true],
    ["/test/me/please", '*/me/*', true],
    ["/test/m1e/please", '*/me/*', false],
    ["/test/me/please", 'test*please', false],
    ["/test/me/please", '/test*please', true],
];

foreach ($testData as $item) {
    if (
        Wildcard::isMatching($item[0], $item[1]) !== $item[2]
    ) {
        throw new Exception("Wildcard::isMatching() failed. Input data: " . PHP_EOL . json_encode($item));
    }
}

# @Wildcard::filterIndexedArray()
$testData = [
    #0
    [
        [
            '/var/www/html/index.php',
            '/var/www/html/test.csv',
            '/var/www/html/test.php',
            '/var/www/html/htdocs/index.php',
            '/var/www/html/htdocs/test.php',
            '/var/www/html/.gitignore',
        ],
        [
            '*index*',
            '*test*'
        ],
        [
            '*htdocs*',
            '*.csv'
        ],
        null,
        true,
        [
            '/var/www/html/index.php',
            '/var/www/html/test.php',
        ],
    ],
    #1
    [
        [
            '/var/www/html/index.php',
            '/var/www/html/test.csv',
            '/var/www/html/test.php',
            '/var/www/html/htdocs/index.php',
            '/var/www/html/htdocs/test.php',
            '/var/www/html/.gitignore',
        ],
        [
            '*index*',
            '*test*'
        ],
        [
            '*htdocs*',
            '*.csv'
        ],
        null,
        false,
        [
            0 => '/var/www/html/index.php',
            2 => '/var/www/html/test.php',
        ],
    ],
    #2
    [
        [
            ['path' => '/var/www/html/index.php'],
            ['path' => '/var/www/html/test.csv'],
            ['path' => '/var/www/html/test.php'],
            ['path' => '/var/www/html/htdocs/index.php'],
            ['path' => '/var/www/html/htdocs/test.php'],
            ['path' => '/var/www/html/.gitignore'],
        ],
        [
            '*index*',
            '*test*'
        ],
        [
            '*htdocs*',
            '*.csv'
        ],
        'path',
        true,
        [
            ['path' => '/var/www/html/index.php'],
            ['path' => '/var/www/html/test.php'],
        ],
    ],
    #3
    [
        [
            ['path' => '/var/www/html/index.php'],
            ['path' => '/var/www/html/test.csv'],
            ['path' => '/var/www/html/test.php'],
            ['path' => '/var/www/html/htdocs/index.php'],
            ['path' => '/var/www/html/htdocs/test.php'],
            ['path' => '/var/www/html/.gitignore'],
        ],
        [],
        [],
        'path',
        true,
        [
            ['path' => '/var/www/html/index.php'],
            ['path' => '/var/www/html/test.csv'],
            ['path' => '/var/www/html/test.php'],
            ['path' => '/var/www/html/htdocs/index.php'],
            ['path' => '/var/www/html/htdocs/test.php'],
            ['path' => '/var/www/html/.gitignore'],
        ],
    ],
];

$i = 0;

foreach ($testData as $item) {
    $result = Wildcard::filterIndexedArray($item[0], $item[1], $item[2], $item[3], $item[4]);
    if (
        $result !== $item[5]
    ) {
        throw new Exception(
            "Wildcard::filterArray() failed. Input data index: #" . $i . PHP_EOL .
            "Actual output: " . PHP_EOL . json_encode($result, JSON_PRETTY_PRINT)
        );
    }
    $i++;
}

echo "success" . PHP_EOL;