README

### ◦ Requirements

- PHP 8:0+
- Linux environment (docker can be used)

### ◦ Start docker container (optional)

---
▸ shell
```shell
docker/run
```

### ◦  Usage

---
▸ shell
```
bin/example
```
